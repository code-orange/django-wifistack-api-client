import requests
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template import loader
from django.utils.translation import gettext as _

from django_template_limitless.django_template_limitless.views import (
    MenuGroup,
    MenuLink,
)


def get_nav():
    nav = list()

    menu1 = MenuGroup()
    menu1.menu_name = "Hardware"

    link1 = MenuLink()
    link1.link_name = "Nodes"
    link1.link_url = "/nodes"
    menu1.menu_items.append(link1)

    nav.append(menu1)

    return nav


def get_nodes_from_api():
    r = requests.get(
        settings.WIFISTACK_API_URL + "/v2/wirelessnode",
        headers={
            "Authorization": "ApiKey "
            + settings.WIFISTACK_API_USER
            + ":"
            + settings.WIFISTACK_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
    )

    return r.json()


def get_node_from_api(node_hostname_fqdn: str):
    if node_hostname_fqdn.startswith("dc-"):
        node_id = node_hostname_fqdn[3:].split(".")[0]

    r = requests.get(
        settings.WIFISTACK_API_URL + "/v2/wirelessnode/" + node_id,
        headers={
            "Authorization": "ApiKey "
            + settings.WIFISTACK_API_USER
            + ":"
            + settings.WIFISTACK_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
    )

    return r.json()


def edit_node_via_api(node_data: dict):
    r = requests.post(
        settings.WIFISTACK_API_URL + "/v2/wirelessnode",
        headers={
            "Authorization": "ApiKey "
            + settings.WIFISTACK_API_USER
            + ":"
            + settings.WIFISTACK_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        json=node_data,
    )

    return r.json()


def view_404(request, exception=None):
    # make a redirect to homepage
    # you can use the name of url or just the plain link
    return redirect("/")  # or redirect('name-of-index-url')


def nodes_list(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/accounts/login/")

    template = loader.get_template("django_wifistack_api_client/nodes_list.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Dolphin Connect")
    template_opts["content_title_sub"] = _("Nodes")

    template_opts["menu_groups"] = get_nav()

    api_data = get_nodes_from_api()
    all_nodes = api_data["objects"]

    template_opts["all_nodes"] = all_nodes

    return HttpResponse(template.render(template_opts, request))


def nodes_edit(request, node_hostname_fqdn):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/accounts/login/")

    template = loader.get_template("django_wifistack_api_client/nodes_edit.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Dolphin Connect")
    template_opts["content_title_sub"] = _("Node Edit")

    template_opts["menu_groups"] = get_nav()

    if request.method == "POST":
        node_data = dict()
        node_data["node_hostname_fqdn"] = node_hostname_fqdn
        node_data["wireless_node_title"] = request.POST["wireless_node_title"]
        node_data["wireless_node_domain"] = request.POST["wireless_node_domain"]
        node_data["wireless_node_geo_lat"] = request.POST["wireless_node_geo_lat"]
        node_data["wireless_node_geo_long"] = request.POST["wireless_node_geo_long"]
        node_data["wireless_node_lan_bridge"] = request.POST["wireless_node_lan_bridge"]
        node_data["wireless_node_lldp_enabled"] = request.POST[
            "wireless_node_lldp_enabled"
        ]
        node_data["wireless_node_mesh_on_lan_enabled"] = request.POST[
            "wireless_node_mesh_on_lan_enabled"
        ]
        node_data["wireless_node_snmp_enabled"] = request.POST[
            "wireless_node_snmp_enabled"
        ]
        node_data["wireless_node_updater_branch"] = request.POST[
            "wireless_node_updater_branch"
        ]
        node_data["wireless_node_updater_enabled"] = request.POST[
            "wireless_node_updater_enabled"
        ]

        edit_node_via_api(node_data=node_data)

    template_opts["node_data"] = get_node_from_api(node_hostname_fqdn)

    return HttpResponse(template.render(template_opts, request))
