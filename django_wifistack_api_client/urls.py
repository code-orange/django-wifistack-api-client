from django.urls import path
from django.views.generic import RedirectView

from . import views

handler404 = "django_wifistack_api_client.django_wifistack_api_client.views.view_404"

urlpatterns = [
    path("nodes", views.nodes_list),
    path("nodes/edit/<str:node_hostname_fqdn>", views.nodes_edit),
    path("dashboard", RedirectView.as_view(url="/nodes", permanent=False)),
    path("", RedirectView.as_view(url="/nodes", permanent=False)),
]
